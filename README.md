## Quest MySQL Database

Database table schemas are in the `schema` directory.

## Setup

* Install [Docker](docker.io)
* Run following commands in Docker Quickstart Terminal

This script will start the MySQL docker and build the database schema.
```
$ ./init.sh
```

Use this script to stop and delete the docker container. All database data will be erased.
```
$ ./kill.sh
```
